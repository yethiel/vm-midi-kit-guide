# Volca Modular MIDI Usage

By default, the Volca responds to MIDI channel 1.

> To play the Volca with a regular keyboard, set the scale on the Volca to Chromatic. The tonic also affects the pitch.

Notes can be played via MIDI that are then quantized to the Volca's scale setting. Set it to chromatic and it will play notes found on a regular chromatic keyboard. If its set to a different scale it will play whatever note of the scale it is closest to. The internal clock also synchronizes to the MIDI clock. Sadly the volca does not respond to pitch-bend and portamento.

The sequencer can still be used The Volca's keyboard overrides MIDI and MIDI overrides the internal sequencer. Notes sent via midi can be recorded by the volca both in normal and step-record mode.

If a note is out of range, it will be wrapped to the closest supported octave.

## CC

Volca Modular responds to the following CCs:

- CC#40: Modulator-carrier ratio
- CC#41: Wave folding amount
- CC#42: Modulation amount
- CC#43: EG 1 attack
- CC#44: EG 1 release
- CC#45: EG2 shape
- CC#46: EG2 time
- CC#47: LPG1 cutoff
- CC#48: LPG2 cutoff
- CC#49: Space amount
