# Links

- [GitHub repository by sensai7 containing PCB source files](https://github.com/sensai7/VolcaModularMidi#volcamodularmidi)
- [Midi Input Mod for Volca Modular on tindie.com](https://www.tindie.com/products/yethiel/midi-input-mod-for-volca-modular/)
- This mod won the [Micro MIDI Modification Award](https://blog.tindie.com/2020/12/tindie-sound-gear-of-the-year-awards-2020/)
- and it was [featured on the Tindie blog](https://blog.tindie.com/2020/08/volca-modular-midi-in/)
- [Video demonstration](https://www.youtube.com/watch?v=Ln6Mao4cA1E)
- [KORG Volca Modular](https://www.korg.com/us/products/dj/volca_modular/)
