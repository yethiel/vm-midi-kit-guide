# Introduction

This mod lets you control the Volca Modular using MIDI.  
In the sidebar you can find guides for assembling and installing it.

![Volca Modular MIDI mod](img/volca.jpg)
