# Volca Modular MIDI Kit Soldering Guide

<!-- toc -->

![Kit parts](img/kit.jpg)

## Instructions

You can find step by step instructions further down. This is an overview of the instructions:

1. Solder the diode to the board. The orientation is important, make sure the marking on the diode matches the one on the board.
2. Solder the resistors to the board. Make sure to match the resistors to the markings on the board. The orientation does not matter. 
    - 220 starts with red-red-…, 
    - 270 starts with red-purple-…
3. Solder the 8-pin header to the board. Do not plug in the IC (optocoupler) yet. Make sure the indent on the socket matches the indent marking on the board.
4. Solder the 4-pin header to the board.
5. Solder the DIN 5-pin (MIDI) jack to the board.
6. Plug in the IC (optocoupler). Match the circle marking on the IC to the indent on the socket. 

## Reference Pictures

![Photo of the assembled board](img/assembled.jpg)

![Photo of the assembled board](img/assembled2.jpg)

## Picture Instructions

### 1. Diode

Match the black marking on the diode with the marking on the PCB.

![Diode and PCB](img/01.jpg)


### 2. Diode

Solder the diode to the PCB.

![Diode and PCB soldered](img/02.jpg)


### 3. Resistors

Match the 270R and 220R resistors. 270R has the markings red-purple-… and 220R has red-red-…

![Resistors and PCB](img/03.jpg)

### 4. Resistors

Solder the resistors to the PCB.

![](img/04.jpg)

### 5. Header

Solder the 4-pin header to the PCB. Tape can be used to hold it in place.

![Bottom of PCB with soldered 4-pin header](img/05.jpg)


### 6. Socket

Match the indent on the 8-pin socket with the marking on the PCB. 

![PCB with 8-pin socket](img/06.jpg)

### 7. Socket

Solder the socket to the board.

![PCB with soldered 8-pin socket](img/07.jpg)

### 8. DIN Jack

Match the 5-pin DIN jack with the top of the PCB. 

Optional: Clean the top of the PCB with isopropyl alcohol to get rid of any flux residue. The jack is going to be installed ontop of some solder joints so they cannot be cleaned after.

![PCB with 5-pin DIN jack](img/08.jpg)

### 9. DIN Jack

Solder the jack to the PCB. Tape can be used to hold it in place.

![PCB with 5-pin DIN jack](img/09.jpg)

### 10. IC

Match the IC's circle marking with the indent of the 8-pin socket.

![PCB with IC](img/10.jpg)

### 11. IC

Gently push the IC into the socket. The pins of the IC might need to be bent into shape to fit into the socket.

At this point I clean the PCB with isopropyl alcohol to get rid of residue.

![PCB with IC installed](img/11.jpg)

### 12. Cable

Connect the cable so that the arrow is visible from the top.

![Board with cable installed](img/12.jpg)

