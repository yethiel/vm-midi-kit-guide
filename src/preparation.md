# Preparation

## Required Tools

### Kit Assembly

- Soldering iron, rosin core solder
- Snippers
- Painter's tape

### Mod Installation

- Soldering iron, rosin core solder
- Snippers, knife or wire stripper
- Painter's tape
- Screwdriver
- Optional:
    - dremel/drill
